// Activity Part 1

console.log("Hello World!");

let firstName = "Lemuel";
console.log("First Name: " + firstName);

let lastName = "Coros";
console.log("Last Name: " + lastName);

let age = 26;
console.log("Age: " + age);

let hobbies = ["Playing video games", "Watching anime", "Reading manga"];
console.log("Hobbies: \n");
console.log(hobbies);

let workAddress = {
	houseNumber : 18,
	street : "C. P. Tinga Street Purok 1-A New Lower Bicutan",
	city : "Taguig City",
	state : "Metro Manila"
};
console.log("Work Address: \n");
console.log(workAddress);

// Activity Part 2 - Debugging

let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		username : "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false
	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);

	let lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);